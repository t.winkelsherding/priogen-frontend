PriogenFrontend.controller('GraphController', ['$scope', '$compile', 'DataService', function ($scope, $compile, DataService) {
    $scope.graphData = {};
    $scope.graphData.intervalSlider = 5;
    $scope.graphData.selectedGraph = null;
    $scope.graphData.startDateSlider = new Date(2015, 0, 1);
    $scope.graphData.selectedDateSlider = new Date(2015, 0, 14);
    $scope.graphData.selectedDateLineGraph = [];
    $scope.graphData.nearbyData = [];
    $scope.graphData.currID = 0;
    $scope.graphData.bBoxSize=200;
    $scope.selectableData =[];

    DataService.getSelectablePresentationData().then(function (result){
        $scope.selectableData = result;
    });
    $scope.graphData.intervalOptions = [
        1, 3, 5, 7, 10
    ];
    $scope.graphData.data = [];

    //@TODO sub selector agnostic select structure (can either be get WHERE name="xxx" or get WEHERE name="xx" and measurement="yyy"
    $scope.selectGraph = function (selectorObj, name) {
        console.log(selectorObj);


        $scope.graphData.currID++;

        if (selectorObj.isSelected(name)) {
            selectorObj.unSelect(name);

            let removeIndex = $scope.graphData.data.map(function(item) { return item.name; })
                .indexOf(name);
            ~removeIndex && $scope.graphData.data.splice(removeIndex, 1);
        } else {
            selectorObj.select(name);
            DataService.getPresentationData(selectorObj.tableName, name,$scope.graphData.currID).then(function(selectedGraph){
                $scope.graphData.data.push(selectedGraph);
            });
        }
    };


    $scope.$watch('graphData.selectedDateLineGraph', function (newVals, oldVals) {
        selectGraphOnMap();
    }, true);
    $scope.$watch('graphData.selectedGraph', function (newVals, oldVals) {
        selectGraphOnMap();
    }, true);

    $scope.$watch('graphData.bBoxSize', function (newVals, oldVals) {
        selectGraphOnMap();
    }, true);
    $scope.$watch('graphData.data', function (newVals, oldVals) {
        if($scope.graphData.data.length ==1){
            $scope.graphData.selectedGraph= $scope.graphData.data[0];
        }
    }, true);

    function selectGraphOnMap(){
        if ($scope.graphData.selectedDateLineGraph.length>1) {
            if ($scope.graphData.selectedGraph != null) {
                console.log();
                let graph = $scope.graphData.selectedGraph;
                DataService.getLatLongPresentationData(graph ,getBBox(graph), $scope.graphData.selectedDateLineGraph,"placeholder").then(function (latLongDataPoints){
                    $scope.graphData.nearbyData= latLongDataPoints;
                })
            }
        }
    }

    //very hacky and rough bBox calculation
    function getBBox(graph) {
        let kmRange = $scope.graphData.bBoxSize;
        console.log(graph);
        return {
            lat1: graph.coordinates[1] - kmRange * 0.0089982311915998,
            lat2: graph.coordinates[1] + kmRange * 0.0089982311915998,
            lon1: graph.coordinates[0] - kmRange * 0.0089982311915998,
            lon2: graph.coordinates[0] + kmRange * 0.0089982311915998
        };
    }
    //$scope.tester = DataService.getPowerPlantData('ee_power_belgium','Amercoeur_1_R_GT');

}]);
