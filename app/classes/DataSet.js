class DataSet {
    constructor(name, dataPoints, id, color, coordinates) {
        this.name = name;
        this.dataPoints = dataPoints;
        this.id = id;
        this.coordinates = coordinates;
        this.color = color;
        this.defaultDomain();
        this.mapSelected = false;
    }
    getMax(){
        return d3.max(this.dataPoints, function (d) {
            return +d.value;
        });
    }
    setDomain(start,end){
        if(start!=null){
            this.startDomain = start;
        }
        if(end!=null){
            this.endDomain = end;
        }
    }
    copyDomain(dataSet){
        this.startDomain = dataSet.startDomain;
        this.endDomain = dataSet.endDomain
    }

    defaultDomain(){
        this.startDomain = 0;
        this.endDomain = this.getMax();
    }
    getColor(){
        return this.color;
    }
    getDomain(){
        return [this.startDomain,this.endDomain];
    }
}