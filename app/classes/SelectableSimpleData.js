class SelectableSimpleData {
    constructor(tableName, availableNames) {
        this.tableName = tableName;
        this.availableNames = availableNames;
        this.selectedNames = [];
    }

    select(name){
        this.selectedNames.push(name);
    }
    unSelect(name){
        let index =  this.selectedNames.indexOf(name);
        if (index !== -1) {
            this.selectedNames.splice(index, 1);
        }
    }
    isSelected(name) {
        return this.selectedNames.indexOf(name) != -1;
    }
    getColor(name){
        if (this.isSelected(name)){
            return "#89f442";
        }else{
            return "";
        }
    }
}