'use strict';

// Declare app level module which depends on views, and components
var PriogenFrontend = angular.module('PriogenFrontend', [

])
    .config(function ($logProvider) {



        $logProvider.debugEnabled(true);

    });
