PriogenFrontend.directive('timeSetter', function () {
//code goes here

    return {
        restrict: 'E',
        scope: {
            startDate: '=start',
            interval: '=',
            data: '=',
            selectedDate: '=sliderSelected'
        },

        template: '<div></div>',
        link: function (scope, element, attrs) {

            let margin = {top: 200, right: 40, bottom: 200, left: 40},
                width = 960 - margin.left - margin.right,
                height = 500 - margin.top - margin.bottom;

            let svg = d3.select(element[0]).append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .attr("class", "timeSlider")
                .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
            scope.$watch('interval', function (newVals, oldVals) {
                return scope.render(scope.data);
            }, true);

            scope.$watch('data', function (newVals, oldVals) {
                return scope.render(scope.data);
            }, true);

            scope.render = function (allGraphs) {

                svg.selectAll('*').remove();

                let startDate = scope.startDate;
                let endDate = new Date(scope.startDate.getFullYear(), startDate.getMonth() + 1, 0);

                let x = d3.scaleTime()
                    .domain([startDate, endDate])
                    .range([0, width])
                    .clamp(true);

                let context = svg.append("g")
                    .attr("class", "context");
                context.append("g")
                    .attr("class", "axis axis--x");

                allGraphs.forEach(function (data) {
                    let y = d3.scaleLinear().range([height, 0])
                        .domain(data.getDomain());
                    let line = d3.line()
                        .curve(d3.curveBasis)
                        .x(function (d) {
                            return x(d.date);
                        })
                        .y(function (d) {
                            return y(d.value);
                        });
                    context.append("path")
                        .datum(data.dataPoints)
                        .attr("class", "line")
                        .attr("stroke",data.getColor())
                        .attr("d", line);
                });
                console.log(startDate);
                console.log(endDate);
                let sliderWidth = x(new Date(scope.startDate.getTime() + ((scope.interval - 1) * 24 * 60 * 60 * 1000)));
                let lastDatePos = x(new Date(endDate.getTime() - ((scope.interval - 1) * 24 * 60 * 60 * 1000)));

                svg.append("g")
                    .attr("class", "axis axis--x")
                    .attr("transform", "translate(0," + height + ")")
                    .call(d3.axisBottom(x)
                        .ticks(d3.timeDay.every(2))
                        .tickPadding(0))
                    .attr("text-anchor", null)
                    .selectAll("text")
                    .attr("x", 6);

                let rect = svg.append("rect")
                    .attr("width", sliderWidth)
                    .attr("height", height)
                    .attr("x", x(scope.selectedDate))
                    .style("opacity", "0.3")
                    .call(d3.drag()
                        .on("start drag", function () {

                            if (d3.event.x > (width - sliderWidth)) {
                                rect.attr("x", lastDatePos)
                            } else {
                                let xPos = x.invert(d3.event.x);
                                let xGrid = d3.timeDay.round(xPos);
                                rect.attr("x", x(xGrid))
                            }
                            scope.$apply(scope.selectedDate = x.invert(rect.attr("x")));
                        }));
            }


        }
    };
});