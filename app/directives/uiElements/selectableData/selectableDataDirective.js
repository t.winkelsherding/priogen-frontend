PriogenFrontend.directive('selectableData',[ function() {
    //code goes here
    //idea of this directive: recieve the tree that is to be displayed from the controller.
    //then make a selectable menu. return the selection to the controller

    return {
        restrict: 'E',
        scope: {
            dataTree: '=treeSelect'
        },
        templateUrl: 'directives/uiElements/selectableData/selectableDataView.html',
        link: function($scope, element, attrs) {
            console.log($scope.dataTree);

        }
    };
}]);