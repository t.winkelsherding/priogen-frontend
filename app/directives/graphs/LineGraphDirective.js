PriogenFrontend.directive('lineGraph', function () {
    //code goes here
    //idea of this directive: recieve the tree that is to be displayed from the controller.
    //then make a selectable menu. return ALL of the selections to the controller

    //noinspection JSUnusedLocalSymbols
    return {
        restrict: 'E',
        scope: {
            interval: '=',
            data: '=',
            intervalStartDate: '=sliderSelected',
            selectedDateLineGraph: '=graphSelected',
        },

        template: '<div></div>',
        link: function (scope, element, attrs) {


            let margin = {top: 10, right: 40, bottom: 20, left: 160},
                width = 960 - margin.left - margin.right,
                height = 500 - margin.top - margin.bottom;


            let svg= createSVG(element, margin ,width, height);
            let yAxisMap = new WeakMap();
            let x = calculateXAndSetAxis(scope.intervalStartDate, scope.interval, svg, width, height);
            yAxisMap = calculateYAndSetAxes(scope.data, yAxisMap, svg, height);
            drawData(scope.data, x, yAxisMap, svg);
            //createBrush(svg, width,height,x );
            let brush = d3.brushX()
                .extent([[0, 0], [width, height]])
                .on("end",  function (){
                    scope.$apply(function(){
                        //noinspection JSUnresolvedVariable
                        scope.selectedDateLineGraph = d3.event.selection.map(x.invert,x);
                    });

                });
            svg.append("g")
                .attr("class", "brush")
                .call(brush);



            //noinspection BadExpressionStatementJS,JSUnusedLocalSymbols
            scope.$watch('data', function (newVals, oldVals) {
                //remove graphs and yAxis and redraw
                yAxisMap = calculateYAndSetAxes(scope.data, yAxisMap, svg, height);
                drawData(scope.data, x, yAxisMap, svg);
            }, true);
            //noinspection BadExpressionStatementJS,JSUnusedLocalSymbols
            scope.$watch('intervalStartDate', function (newVals, oldVals) {
                //remove graphs and redraw
                x = calculateXAndSetAxis(scope.intervalStartDate, scope.interval, svg, width, height);
                drawData(scope.data, x, yAxisMap, svg);
            }, true);
            //noinspection BadExpressionStatementJS,JSUnusedLocalSymbols
            scope.$watch('interval', function (newVals, oldVals) {
                //remove graphs and redraw
                x = calculateXAndSetAxis(scope.intervalStartDate, scope.interval, svg, width, height);
                drawData(scope.data, x, yAxisMap, svg);
            }, true);
        }

    };



   function createSVG(element, margin ,width, height) {

       return d3.select(element[0]).append("svg")
           .attr("width", width + margin.left + margin.right)
           .attr("height", height + margin.top + margin.bottom)
           .attr("class", "lineGraph")
           .append("g")
           .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
   }


    function calculateYAndSetAxes(dataSets, yAxisMap, svg, height) {
        svg.selectAll(".yAxis").remove();
        let helperMap = new WeakMap();
        let i = 0;
        dataSets.forEach(function (data) {
            let y = d3.scaleLinear().range([height, 0])
                .domain(data.getDomain());
            if (helperMap.has(data.getDomain())) {
                d3.select().attr("id", "y-" + helperMap.get(data.getDomain()))
                    .attr("stroke", "black")
            } else {
                helperMap.set(data.getDomain(), i);
                svg.append("g")
                    .attr("class", "yAxis")
                    .attr("id", "y-" + i)
                    .styles({
                        fill: "none", "stroke": data.getColor(), "stroke-width": "1"
                    })
                    .attr("transform", "translate(-" + i * 28 + ",0)")
                    .call(d3.axisLeft(y)
                        .ticks(5));
                i++;
            }
            yAxisMap.set(data, y);

        });
        return yAxisMap;
    }

    function drawData(dataSets, x, yAxisMap, svg) {
        svg.selectAll(".line").remove();
        dataSets.forEach(function (data) {
                y = yAxisMap.get(data);


            let line = d3.line()
                .curve(d3.curveBasis)
                .x(function (d) {
                    return x(d.date);
                })
                .y(function (d) {
                    return y(d.value);
                });
            svg.append("path")
                .datum(data.dataPoints)
                .attr("class", "line")
                .attr("stroke", data.getColor())
                .attr("d", line);
        });
    }

    function calculateXAndSetAxis(intervalStartDate, interval, svg, width, height) {
        svg.selectAll(".xAxis").remove();
        let lastDatePos = new Date(intervalStartDate.getTime() + ((interval - 1) * 24 * 60 * 60 * 1000));
        x = d3.scaleTime()
            .domain([intervalStartDate, lastDatePos])
            .range([0, width])
            .clamp(true);
        svg.append("g")
            .attr("class", "xAxis")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x));
        return x;
    }


        //focus.attr("transform", "translate(" + x(d.date) + "," + y(d.close) + ")");
        //focus.select("text").text(formatCurrency(d.close));

});