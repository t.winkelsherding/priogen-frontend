PriogenFrontend.directive('map', function () {

    let backGroundLayer = new ol.source.Vector();

    let entityVector = new ol.source.Vector();

    let countryLayer = new ol.layer.Vector();
    let controlAreaLayer = new ol.layer.Vector();
    let mapLayerGroup = new ol.layer.Group();
    let overlayLayer = new ol.layer.Image();

    //for some reason we cannot initialize the map here, we need to do it later
    let map = {};
    let images = {
        up: new Image(),
        down: new Image(),
        equal: new Image(),
        flash: new Image(),
    };



    return {
        restrict: 'E',
        scope: {
            selectedGraph: '=',
            selectedLayers: '=',
            nearbyData: '='
        },

        template: '<div  id="map" class="map"></div>',
        link: function (scope, element, attrs) {
            initMap();
            //addFeatures();

            scope.$watch('nearbyData', function (newVals, oldVals) {
                if (scope.selectedGraph != null) {
                    console.log(scope.nearbyData);
                    console.log(scope.nearbyData);

                    setElement(scope.selectedGraph, scope.nearbyData  );
                }
            }, true);

            function setElement(graph, nearby) {
                entityVector.clear();
                let canvasFunction = function (extent, resolution, pixelRatio,
                                               size, projection) {

                    let canvasWidth = size[0];
                    let canvasHeight = size[1];
                    let canvas = d3.select(document.createElement('canvas'));
                    canvas.attr('width', canvasWidth).attr('height', canvasHeight);
                    let context = canvas.node().getContext('2d');
                    let d3Projection = d3.geoMercator().scale(1).translate([0, 0]);
                    let scale = 6378137 / (resolution / pixelRatio);

                    let center = ol.proj.transform(ol.extent.getCenter(extent),
                        projection, 'EPSG:4326');
                    d3Projection.scale(scale).center(center)
                        .translate([canvasWidth / 2, canvasHeight / 2]);


                    nearby.forEach(function(data, i) {
                        let projected = d3Projection(data.coordinates);

                        context.drawImage(images.flash, projected[0]-8,projected[1]-8, 32,32);
                        context.fill();
                        context.closePath();
                        context.stroke();
                        let lastVal="placeholderxhfuaohfuo";
                        let j=2;
                        let lastScale = 16;
                        data.dataPoints.forEach(function(dataPoint, i) {
                            let imageToDraw=new Image(0);
                            if(lastVal!="placeholderxhfuaohfuo"){

                                scale = (lastVal-dataPoint.value)/lastVal;
                                let imageScale=16*(Math.abs(scale)+1);
                                if(imageScale>32) imageScale = 32;
                                if(scale >0.5){
                                    imageToDraw=images.up;
                                }else if(scale <-0.05){
                                    imageToDraw=images.down;
                                    imageScale = 16;
                                }else{
                                    imageToDraw=images.equal;
                                }

                                context.drawImage(imageToDraw, projected[0]+8-imageScale/2,projected[1]+(i*12), imageScale,imageScale);
                                context.stroke();

                                //console.log("lastscale"+lastScale+", i "+i +"  " +i*lastScale);
                                lastScale = imageScale;
                            }

                            lastVal =dataPoint.value;
                            j++;

                        });

                    });
                    return canvas._groups[0][0];

                };
                nearby.forEach(function(data, i) {
                    let feature = createFeature(data);
                    entityVector.addFeature(feature);
                });

                let feature = createFeature(graph);
                entityVector.addFeature(feature);

                map.getView().animate({zoom: 6, center: ol.proj.fromLonLat(graph.coordinates), duration: 300});
                overlayLayer.setMap(null);
                overlayLayer = new ol.layer.Image({
                    source: new ol.source.ImageCanvas({
                        canvasFunction: canvasFunction,
                        projection: 'EPSG:3857'
                    })
                });
                overlayLayer.setMap(map);

            }

        }
    };
    function initMap() {


        images.up.src = "assets/arrow_up.svg";
        images.down.src = "assets/arrow_down.svg";
        images.equal.src = "assets/equal.svg";
        images.flash.src = "assets/flash.svg";


        backGroundLayer = new ol.layer.Tile({
            source: new ol.source.OSM({
                url: "http://{a-c}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png"
            })
        });
        countryLayer = new ol.layer.Vector({
            opacity: 0.5,
            source: new ol.source.Vector({
                format: new ol.format.GeoJSON(),
                url: 'assets/europe.geojson'
            })
        });
        countryLayer.setStyle();
        controlAreaLayer = new ol.layer.Vector({
            opacity: 0.5,
            source: new ol.source.Vector({
                format: new ol.format.GeoJSON(),
                url: 'assets/controlAreas.geojson'
            })
        });
        let entityLayer = new ol.layer.Vector({
            source: entityVector
        });

        map = new ol.Map({
            target: 'map',
            view: new ol.View({
                center: ol.proj.fromLonLat([4.896372, 50.60240]),
                zoom: 6
            }),
            layers: [
                backGroundLayer,
                // countryLayer,
                // controlAreaLayer,
                entityLayer

            ]
        });




    }



    function calculateFeatureIcon() {

    }


    function addLayer() {

    }

    function createFeature(graph) {
        let feature = new ol.Feature({
            geometry: new ol.geom.Point(ol.proj.fromLonLat(graph.coordinates))
        });
        feature.setStyle(new ol.style.Style({
            image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
                color: graph.color,
                src: 'assets/dot.png'
            }))
        }));
        return feature;
    }


});
/**
 * Created by tobi on 17-1-17.
 */
