PriogenFrontend.factory('DataService', ['$http', function ($http) {
    //@Todo this whole class is just pseudocode and needs debugging
    /**
     * Tobis comment here:
     * this class was supposed to handle the communication with the backend.
     * the backend never got done
     * hell the backend was supposed to take 2 weeks...
     * so now it only loads json files...
     */
    //object with style object['tableName']['attributeName']
    /*
     var currentActiveData;
     var currentTimeRange;

     var resolveActiveData = function (tableName, attributeName, timeline) {
     //get reuqest with timeline, tablename and attribute name
     $http.get('localhost:8080//queryToDataStore')
     .then(function (result) {
     //resolve the promise as the data
     currentActiveData[tableName][attributeName] = result;
     return currentActiveData;
     });
     };


     */


    return {

        //This is a request to return a single dataSet to the Frontend.
        getMeteomanzData: function (TableName, Selector, Limitor) {
            $http({
                url: "http://localhost:8080/web_war_exploded/ServletTest",
                method: "GET",
                params: {TableName: TableName, Measurement: [Selector], StationName: [Limitor]}
            }).then(function (result) {
                //resolve the promise as the data
                console.log(result.data)
                return result.data;
            })

        },

        getPowerPlantData: function (TableName, Selector) {
            $http({
                url: "http://localhost:8080/web_war_exploded/ServletTest",
                method: "GET",
                params: {TableName: TableName, PowerplantName: [Selector]}
            }).then(function (result) {
                //resolve the promise as the data
                console.log(result.data);
                return result.data;
            })

        },

        //This is a request to return a single dataSet to the Frontend.
        getPresentationData: function (tableName, itemName, id) {
            return $http.get('assets/ee_power_germany.json').then(function (response) {
                //plain loop as this needs to be synchronous
                for (let i = 0; i < response.data.length; i++) {
                    let loaded = response.data[i];
                    if (loaded.name === itemName) {
                        let parsedVals = [];
                        loaded.values.forEach(function (eachVal) {
                            let h = {
                                date: new Date(eachVal.date),
                                value: eachVal.value
                            };
                            parsedVals.push(h);
                        });
                        console.log(loaded.name);
                        return new DataSet(loaded.name, parsedVals, id, d3.interpolateRainbow(id / 5), [loaded.longitude, loaded.latitude])
                    }
                }
            });
        },
        getLatLongPresentationData: function (graph,bBox, timeFrame, mapLayers) {
            console.log(timeFrame);
            return $http.get('assets/ee_power_germany.json').then(function (response) {
                //plain loop as this needs to be synchronous
                let nearbyData = [];
                for (let i = 0; i < response.data.length; i++) {
                    let loaded = response.data[i];
                    if(graph.name!=loaded.name){
                        if (loaded.longitude > bBox.lon1 && loaded.longitude < bBox.lon2 && loaded.latitude > bBox.lat1 && loaded.latitude < bBox.lat2) {
                            let arr = [];
                            loaded.values.forEach(function (singleData) {
                                if (new Date(singleData.date) > timeFrame[0] && new Date(singleData.date) < timeFrame[1]) {
                                    arr.push(singleData);
                                }
                            });
                            nearbyData.push(new DataSet(loaded.name, arr, graph.id, "grey", [loaded.longitude, loaded.latitude]));
                        }
                    }

                }
                return nearbyData;
            });

        },
        /**
         * ASYNC
         * returns the data for all selectable items.
         * Should be returned from the backend
         */
        getSelectablePresentationData: function () {

            return $http.get('assets/ee_power_germany.json').then(function (response) {
                let availableNames = [];

                response.data.forEach(function (loaded) {
                    availableNames.push(loaded.name);
                });
                return [new SelectableSimpleData("ee_power_germany", availableNames)];
            });
        }
    }

}]);

